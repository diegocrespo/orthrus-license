# Orthrus License

This is a project that contains a proposal for The Orthrus License. The Orthrus license is a dual-tier software license. It offers a Community Tier similar to the Apache 2.0 license for certain users and a Professional Tier for larger organizations or those with significant resources. It is named after [Orthrus](https://en.wikipedia.org/wiki/Orthrus) the two headed brother of Cerberus. This is merely a draft, the license has not been reviewed by a lawyer
