# FAQ for the Orthrus License

## What is the Orthrus License?
The Orthrus License is a dual-tier software license designed to facilitate use by various groups, including individual developers and larger organizations, with different terms based on size and resources.

## Who can use the Community Tier?
Eligible users include individual developers, open source projects using this or a GPL-compatible license, academic researchers, educators, registered non-profits, and small professional teams (up to 25 members).

## What are the Community Tier restrictions?
Organizations with more than 25 full-time developers, more than 100 PCs, or over $1 million in annual revenue are excluded.

## Can I modify the software under the Community Tier?
Yes, users are free to modify or enhance the software and distribute both original and modified versions.

## Do I retain my patent rights if I contribute?
Contributors grant others the right to use their patents related to their contributions but retain ownership.

## What if my organization grows beyond the Community Tier limits?
Organizations exceeding the limits must switch to the Professional Tier.

## How does the Professional Tier work?
It is for larger organizations, with eligibility based on size and revenue. Terms, including pricing, are negotiated with the software developer.

## Are there exceptions to the Professional Tier eligibility?
Yes, open source projects with GPL-compatible licenses, registered non-profits, academic institutions, and individual developers are exempt, even if they meet the size or revenue criteria.

## Is there a warranty with the Orthrus License?
No, the software is provided "as is," without warranties.

## Can I use the software's trademarks?
No, the license does not grant rights to use any trademarks associated with the software.

## What if the Orthrus License terms are updated?
Typically, the applicable version is the one in effect when you began using the software, unless specified otherwise by the project maintainers.
